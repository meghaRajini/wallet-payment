package org.training.walletpayment.entity;

import java.util.List;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.Data;

/**

* @author Nadiya
* @param cartId which describes the cart Id
* @param  user which describes the User
* @param  totalprice  which describes the  Total Price
* @param productQuantities which describes the available quantity of a product
* 
*
*/

@Entity
@Data
public class Cart {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int cartId;
	
	@ManyToOne
	@JoinColumn(name = "userId")
	private User user;
	
	private double totalPrice;
	
	@OneToMany
	private List<ProductQuantity> productQuantities;
}
