package org.training.walletpayment.controller;

import java.util.List;

import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.training.walletpayment.dto.ProductDto;
import org.training.walletpayment.service.ProductService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

/**
 * 
 * The ProductsController class represents the REST API endpoint that handles
 * requests related to retrieving products.
 */
@RestController
@RequestMapping("/")
public class ProductController {
	
	@Autowired
	   RestTemplate restTemplate;
	/**
	 * 
	 * The service object used to manage the product-related business logic.
	 */

	@Autowired
	private ProductService service;

	/**
	 * 
	 * Retrieves all products whose name contains the provided search term, and
	 * returns a paginated response.
	 * 
	 * @param productName the search term to be used to find matching products (value should
	 *                    be at least 3 characters long)
	 * 
	 * @param page        the page number to be retrieved (value should be greater than or
	 *                    equal to 0)
	 * 
	 * @param pageSize    the maximum number of products to be returned per page
	 *                    (value should be greater than 0)
	 * 
	 * @return a ResponseEntity object containing a list of ProductDto objects
	 *         representing the products that match the search term, with an HTTP
	 *         status of 200 (OK)
	 */
	@Operation(summary="Search product by product name")
	@ApiResponses(value= {
			@ApiResponse(responseCode = "201",description="Product found",content= {
					@Content(mediaType ="application/json")
			})
	})

	@GetMapping("/products")
	public ResponseEntity<List<ProductDto>> getALL(@RequestParam @Size(min = 3) String productName,
			@RequestParam @Min(value = 0, message = "Value must be greater than or equal to 0") int page,
			@RequestParam @Min(value = 1) int pageSize) {

		return new ResponseEntity<List<ProductDto>>(service.findByProductNameContaining(productName, page, pageSize),
				HttpStatus.OK);
		
		   }

	}
