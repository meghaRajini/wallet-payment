package org.training.walletpayment.feignclient;

import java.util.List;

import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.training.walletpayment.dto.ProductDto;
@FeignClient(value = "BankService", url = "http://localhost:8082/user/users")
public interface Order {
	@PostMapping("/{userId}")
	public List<ProductDto> getAll(@RequestParam @Size(min = 3) String productName,
			@RequestParam @Min(value = 0, message = "Value must be greater than or equal to 0") int page,
			@RequestParam @Min(value = 1) int pageSize);
}
